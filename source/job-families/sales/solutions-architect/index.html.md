---
layout: job_family_page
title: "Solutions Architect"
---

Solutions Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address client business requirements. Solutions Architects are responsible for driving and managing the technology evaluation and validation stages of the sales process. Solutions Architects are the product advocates for GitLab’s Enterprise Edition.  They focus on the technical solution while also understanding the business outcomes the customer is trying to achieve.

The Solutions Architect helps drive value and change with software development for one of the fastest-growing platforms. By applying solution selling and architecture experience from planning to monitoring, the Solutions Architect supports and enables successful adoption of the GitLab platform. Solutions Architects work with GitLab's top enterprise customers. Solutions Architects work collaboratively with Sales, Engineering, Product Management, and Marketing organizations.

This role provides technical guidance and support throughout the entire sales cycle. Solution Architects can help shape and execute a strategy to build mindshare and broad use of the GitLab platform with customers by becoming the trusted advisor. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting. Proficiency in connecting technology solutions to measurable business value is critical to a Solutions Architect. Candidates should also have a demonstrated skill to think strategically about business, products, and technical problems.

To learn more, see the [Solutions Architect handbook](/handbook/customer-success/solutions-architects)

## Solutions Architect

### Job Grade
The Solutions Architect is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

## Responsibilities

* Engage with customers, both onsite and remote, in a technical consultancy and advisor role during the pre-sales process while providing technical assistance and solution guidance.
* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue targets through the adoption of GitLab.
* With comprehensive knowledge of the GitLab platform and associated technologies, educate customers of all sizes on the value proposition of GitLab while participating in discussions throughout the organization to ensure successful GitLab deployment.
* Guide technical evaluations via POC/POV ownership, RFP/audit support, and workshop design.
* Build deep relationships with people within customer environments to enable them to be GitLab advocates.
* Serve as the customer advocate to other GitLab teams, including Product Development, Sales, and Marketing.
* Continuously improve your professional skills with a focus on personal mastery and team learning.

## Requirements

* Technical presentation and communication skills
* Experience with technical pre-sales or as a professional in the field of information technology
* Knowledge of the end-to-end software development lifecycle
* Understanding of continuous integration and continuous deployment
* Experience with modern software development or operations and their associated technologies
* Experience with cloud computing and related technologies and practices
* Willingness to travel
* B.Sc. in Computer Science or equivalent experience
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks)
* Ability to use GitLab

### Performance Indicators

As with all roles in the Sales Department, the Solutions Architect participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

---

* TOC
{:toc .hidden-md .hidden-lg}

## Levels

### Associate Solutions Architect

#### Job Grade
The Associate Solutions Architect is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities
* Same responsibilities as a Solutions Architect

#### Requirements
* Same requirements as a Solutions Architect but requires less experience

#### Performance Indicators
As with all roles in the Sales Department, the Associate Solutions Architect participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Senior Solutions Architect

#### Job Grade
The Senior Solutions Architect is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities

* Extends the Solutions Architect responsibilities
* Solve technical customer issues of broad scope and high complexity.
* Provide mentorship for Solution Architecture team members and remain a current contributor to team-learning initiatives and activities.
* Work cross-departmentally to find solutions to complex scenarios and integration issues.
* Propose improvements and innovation for customer calls and product demonstrations based on current market trends.
* Maintain in-depth knowledge of the entire GitLab application.
* Represent GitLab as a speaker at field events or as an author in GitLab-focused publications and blogs.
* Provide opportunity strategy leveraging market and industry knowledge and trends.
* Collaborate with the product team while representing customer requirements and feedback.
* Coach sales team-members on deal qualification when necessary.
* Assists with specific objectives and key result associated tasks

#### Requirements
* Extends the Solutions Architect requirements
* Has experience with the additional responsibilities of a Senior Solutions Architect


#### Performance Indicators
As with all roles in the Sales Department, the Senior Solutions Architect participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Staff Solutions Architect

#### Job Grade
The Staff Solutions Architect is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities

* Extends the Senior Solutions Architect responsibilities
* Build and deliver high-value and reusable enablement content that measurably improves the team's effectiveness and execution.
* Sustain mentorship for specific members of the Solutions Architecture team to improve our technical win rate and increase iACV.
* Guide Solution Architect team members through complex customer opportunities and activity.
* Deliver product roadmap discussions.
* Architect innovative solutions leveraging 3rd party and GitLab technologies that lead to new use cases and revenue opportunities.
* Maintain a positive personal brand through GitLab-relevant participation in channels such as conferences, blogs, social media, and other publications.

#### Requirements
* Extends the Senior Solutions Architect requirements
* Has experience with the additional responsibilities of a Staff Solutions Architect


#### Performance Indicators
As with all roles in the Sales Department, the Staff Solutions Architect participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Principal Solutions Architect

#### Job Grade
  The Principal Solutions Architect is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities

* Extends the Staff Solutions Architect responsibilities
* Create and maintain relationships with customer executives that contribute to large iACV deals and customer success.
* Collaborate with product marketing, engineering, and management on a market strategy that results in an improved win rate.
* Exert influence on the overall objectives, key results, and other long-range measurable goals of the team.
* Translate 3rd party integrations into reference architectures and effectively communicates the design and it's value to customer and GitLab team members.
* Sustain assistance requests by GitLab team members as a pre-sales and technical solutions authority for the top 10% of our iACV opportunities.

#### Requirements
* Extends the Staff Solutions Architect requirements
* Has experience with the additional responsibilities of a Principal Solutions Architect


#### Performance Indicators
As with all roles in the Sales Department, the Principal Solutions Architect participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Manager, Solutions Architects

#### Job Grade
The Manager, Solutions Architects is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

GitLab is a hyper-growth company searching for diligent, assured and agile skills in technology, sales, business, communication and leadership. Desire to lead through change is a must.

The Manager, Solutions Architects role is a management position on the front lines. The manager is a player/coach role where the manager is experienced with and has advanced insight to the GitLab platform. The manager will contribute to the territory and account strategy as well as driving the execution. The manager will need to be very comfortable giving and receiving positive and constructive feedback, and adapting to environmental change and retrospecting on successes and failures. The Manager, Solutions Architects will work with the other managers within the customer success organization to help execute strategies and vision with the Director.

#### Responsibilities

- Understand and evangelize GitLab's vision, strategy, and values to your team while also translating them for application to team-specific scenarios.
- Grow a highly effective team through hiring, coaching, and retention of personnel.
- Provide continuous feedback to your team members as well as peers and leaders.
- Be accountable for the overall execution of your team while tracking key performance indicators (KPIs).
- Provide clear communication to your team, peers, and leadership on all essential matters.
- Guide by example while performing the responsibilities of a solution architect to drive customer success.
- Work closely with regional sales managers on strategy.


#### Requirements

- Extends the Solutions Architect requirements
- Experienced in mentoring people, including giving and receiving constructive feedback
- Experienced in collaborating with other managers and executing strategies
- Previous leadership experience is a plus

#### Performance Indicators
As with all roles in the Sales Department, the Manager of Solutions Architects participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Senior Manager, Solutions Architects

#### Job Grade
The Senior Manager, Solutions Architects is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities

- Extends the Manager, Solutions Architect responsibilities.
- Be the directly responsible person for multiple, high-impact initiatives.
- Provide cross-functional leadership on GitLab's most strategic revenue opportunities.
- Collaborate with SA and cross-departmental leadership to iterate on and improve efficiencies of GitLab practices and execution strategies.

#### Requirements

- Extends the Solutions Architect requirements
- Previous leadership experience with a proven track record of success.
- Previous leadership experience at GitLab is a plus.


#### Performance Indicators
As with all roles in the Sales Department, the Senior Manager of Solutions Architects participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Director, Solutions Architects

#### Job Grade
The Director, Solutions Architects is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

GitLab’s Director, Solution Architects provides strategic vision and builds and develops an exceptional team of sales-focused Solution Architects. This person manages the team responsible for leading technical evaluation and validation of prospects’ and customers’ needs to drive new customer acquisition and expansion for existing customers. Partnering closely with Sales and Customer Success teams, this leader will create strategies and operations to effectively and efficiently accelerate GitLab’s growth and improve prospects’ and customers’ experiences.

#### Responsibilities

- Hire, mentor and develop an exceptional team of Solutions Architects.
- Develop strategies and operations to improve win rates to discovering, articulating, and demonstrating GitLab's solution to deliver on specific customer requirements and desired business outcomes.
- Identify and lead initiatives and programs to scale the organization and its operations for future growth.
- Develop processes and metrics and KPIs to improve effectiveness and efficiency of technical evaluations, workshops, demos, and proof-of-concept engagements.
- Partner with Sales, Channels and Alliances teams to align on overall strategy and priorities and provide support for specific prospects, customers and partners.
- Develop and foster relationships for key customers at the technical sponsor and executive level.
- Partner with sales leadership to align with and deliver to regional and account plans, strategies and quarterly goals.
- Collaborate with Sales and Customer Success to improve engagement models and ensure the appropriate coverage of prospects and customers.
- Partner with Product, Engineering, Marketing and Services teams to provide feedback to improve products, services and value messaging based on field experiences and feedback.
- Partner with Sales Operations to ensure efficient and ongoing enablement and development of the team.
- Be a role model for GitLab’s values and culture.

#### Requirements

- 5+ years of experience leading technical sales teams (i.e., Solutions Architect and or Sales Engineering teams)
- 2+  years of experience building and leading global teams of managers with team sizes of 30+ team members
- Demonstrated proficiency building and improving strategies and operations to technical assessment processes and team enablement
- Experience with software development lifecycle processes and tools as well as agile and or DevOps practices
- Knowledgeable with cloud technologies (e.g., Kubernetes, Docker), application security (SAST, DAST) and or cloud deployment models (AWS, GCP, Azure)
- Experience selling technical solutions to technical staff, management, and executive stakeholders
- Proven experience partnering with the broader organization (sales, channel, alliances, product and engineering, marketing and customer success)
- B.S. in Computer Science, Engineering or equivalent experience


#### Performance Indicators
As with all roles in the Sales Department, the Director of Solutions Architects participates in a subset the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

### Senior Director, Solutions Architects

#### Job Grade
The Senior Director, Solutions Architects is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Responsibilities
- Extends the Director, Solutions Architect responsibilities

#### Requirements
- Extends the Director, Solutions Architect requirements
- 7+ years of experience leading technical sales teams (i.e., Solutions Architect and or Sales Engineering teams)
- 4+ years of experience building and leading global teams of managers and directors with team sizes of 50+ team members


#### Performance Indicators
As with all roles in the Sales Department, the Senior Director of Solutions Architects participates in a subset of the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)


---
## Specialties

### Channel Solution Architect

##### Responsibilities

* Primarily engaged in a technical consultancy role for channel partners, providing technical assistance and guidance specific to the selling and service delivery readiness of Gitlab Channel partners.
* In partnership with the channel sales team, formulate and execute a sales, solution and practice strategy to exceed revenue targets through the delivery of solutions & services that drive the adoption of GitLab.
* Educate channel partners of all sizes on the value proposition of GitLab, and participate in all levels of discussions throughout the organization to ensure our solution is set up for successful deployment.
* Work on­site with strategic channel partners, delivering solutions architecture consulting, technical guidance, knowledge transfer, and earn “trusted advisor status.”
* Enable the partners to provide technical evaluations via POC/POV ownership, RFP/audit support, and workshop design.
* Capture and share best-practice knowledge amongst the GitLab community and other channel solution architects.
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, diagrams, or the GitLab Handbook.
* Build deep relationships with senior technical people within channel environments to enable them to be GitLab advocates.
* Serve as the customer advocate to other GitLab teams, including Product Development, Sales, and Marketing.
* Present GitLab platform strategy, concepts, and roadmap to technical leaders within channel partner organizations.

##### Requirements

* Extends the Solutions Architect requirements
* Experience building solutions and professional services through channel partnerships
* Knowledge of all or most of the following channel services categories in the DevOps space: Consulting, Managed and or Advisory services

### Alliances Solution Architect

##### Responsibilities

* Build and deliver demos that highlight the Alliance partners technologies as they work with GitLab
* Assist the sales SA team with customer engagements that involve coordination between alliance partner technologies.
* Educate the Customer Success team on the Alliance team initiatives on how GitLab / Alliance partner technology provide value collectively.
* Build deep relationships with senior technical people within partnerships to enable them to be GitLab advocates.
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, diagrams, or the GitLab Handbook.

##### Requirements
* Extends the Solutions Architect requirements
* Technical background with key partnerships such as AWS, GCP, VMware, and Hashicorp. Certification in AWS/GCP is a plus
* Ability to breakdown Alliance initiatives into tasks and drive them forward

### Public Sector Solutions Architect

#### Job Grade 

The Public Sector Solutions Architect is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

* Extends the Solutions Architect responsibilities.
* Participate in conferences and trade shows and interacting with government customers in attendance.
* Contribute to the creation of case studies, white papers, and media articles for government customers and or partner.
* Consistently provide world-class customer service during pre-sales, implementation, and post-sales activities.
* Manage all technical aspects of the sales cycle: creating high-quality professional presentations, custom demos, proof of concepts, deliver technical deep-dive sessions & workshops, differentiate GitLab from alternative solutions, answering RFI, RFPs, etc.
* TS/SCI Security Clearance
* Must be located in the Washington DC metro area
* Knowledge and at least four years of experience with Federal customers
* Ability to travel up to 50%
* Understand mono-repo and distributed-repo approaches.

#### Requirements

* Extends the Solutions Architect requirements

### Commercial Solutions Architect

#### Responsibilities

* The responsibilities for a Commercial Solutions Architect are the same as a Solutions Architect but support the [Commercial Sales](https://about.gitlab.com/handbook/sales/commercial/) by focusing on [SMB and Mid-Market](https://about.gitlab.com/handbook/business-ops/resources/#segmentation) customers.

#### Requirements

* The requirements for a Commercial Solutions Architect are the same as a Solutions Architect.

## Performance Indicators
As with all roles in the Sales Department, the Solutions Archtiect job family participates in a subset the [Sales KPIs](/handbook/business-ops/data-team/kpi-index/#sales-kpis).

* [IACV versus Plan](https://about.gitlab.com/handbook/sales/performance-indicators/#iacv-vs-plan)
* [Win Rate](https://about.gitlab.com/handbook/sales/performance-indicators/#win-rate)

## Career Ladder
A Solution Architect can progress through the various individual contributor levels or leadership roles and associated levels.

---
## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

### Individual Contributor Hiring Process:

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
- Next, candidates will be invited to schedule a first interview with the SA team Manager.
- Candidates may be invited to schedule an interview with a Solutions Architect peer or other SA team Manager.
- Then, candidates will be required to deliver a demo of GitLab to a panel of Customer Success attendees using the [Demo Guide](https://docs.google.com/document/d/12Dw4p25R5FaLnLpwFGtEr0kxxoWklWJL9FyP-NTITKY/edit?ts=5c48c337).
- Candidates may be invited to additional interviews.
- Successful candidates will be made an offer after references are verified.

### Management Hiring Process:

- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters.
- Next, candidates will be invited to schedule a first interview with a Customer Success VP or Director.
- Candidate will then schedule an interview with an SA management peer.
- Then, candidates will present a business plan to include 30/60/90 day approach, outcomes, and metrics.
- Candidates may be invited to additional interviews.
- Successful candidates will be made an offer after references are verified.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).

---
