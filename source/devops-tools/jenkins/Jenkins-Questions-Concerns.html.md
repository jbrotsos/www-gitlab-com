---
layout: markdown_page
title: "Your Jenkins Questions and Concerns"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->

# On this page
{:.no_toc}

- TOC
{:toc}

## Maintenance and Scalability Concerns

|     Concerns You May Have     |                                                                                                                     Questions To Ask                                                                                                 |
|:----------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|          Dedicated Resources |                                                                      How do we build and maintain pipelines, manage machines, install tools etc.?                                                                      |
| Specialized Groovy Expertise |                                       I will need people with Groovy knowledge to write scripts in Jenkins. Who are my Groovy experts and have they been with our company long?                                        |
|             Plug-in Security |                      Use of third party plugins to access Jenkins module will create exposure?  How will we test these plug-ins?  How will we maintain access permissions to the Jenkins modules?                      |
|          Upgrades are Risky  | Because of a daisy chain of plug-ins and custom development, upgrades cause downtime.  How much downtown time will we have from upgrades?  How often do we upgrade Jenkins?<br>What process do we follow for upgrades? |

## Enterprise Readiness Concerns

|    Concerns You May Have   |                                                                                  Questions To Ask                                                                                  |
|:-------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| UI using single container |                    Jenkins UI is run on a single container with no failover or clustering.  What’s our scaling and failover set up? What kind of uptime do we target?                    |
|         Lack of Analytics |             Jenkins needs to rely on plug-ins for analytics.  What are the metrics that we track?  (Release velocity, Lead time from Issue to Deployment, Bugs per release?)             |
|     Audits and Compliance | With Jenkins, it's not easy to track all that occurred  in a release.  If I'm in a regulated industries, what are our audit and compliance requirements? How often do we need to report? |

## Too Many Community PlugIns Concerns

| Concerns You May Have |                                                                                   Questions To Ask                                                                                   |
|:---------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|      Too Many PlugIns |           With Jenkins it can be hard to find what you need, sometimes there are 30+ plugins for a single action, how to choose?  How many plugins do we use?  For what purpose?           |
|   PlugIn Dependencies |              Jenkins upgrades may depend on many plugins, some may not be compatible which could slow upgrades.  How often do would we upgrade plugins and the Jenkins server?             |
|        PlugIn Quality | Jenkins has received complaints of poor plugin quality and lack of maintenance.  Do we actively monitor our plugins and certify?  What is the process to validate new versions of plugins? |

## Higher OpEx Concern

* We use Jenkins Open Source.  Moving to GitLab CI will increase our operational cost requiring a subscription and new expertise

|                                                                                                                 True Cost                                                                                                                |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|                                   Maintaining Jenkins often requires a minimum time commitment of 10% of a developer's times (10% of their salary dedicated to Jenkins)..remember this is for CI alone                                   |
|                                                                GitLab CI reduces build times which contributes to delivering quality code faster which leads to lower OpEx                                                               |
|                               GitLab’s single application offers easily consumable UI knobs and system features (We can Demo this for you!), presenting a quick and easy (i.e. affordable) learning curve.                               |
| Deep Jenkins technical expertise will require CloudBees which is offered through a paid subscription plan.  GitLab offers a free tier and includes support within the paid tiers                                                         |
| Plugins with Jenkins could cost you in downtime due to outdated Plugins or security vulnerability introduced after upgrading.  GitLab offers one end-to-end software delivery application that embeds enterprise grade security features |

## Decision Silos Concern

* We allow each team to select their own DevOps tools.  We don’t want to force one tool across the organization.

|                                     Cross team collaboration                                     |                                                                                                                                    |
|:------------------------------------------------------------------------------------------------:|------------------------------------------------------------------------------------------------------------------------------------|
| GitLab improves synergies across the company by providing a single application that accelerates: | Sharing of ideas<br>Providing feedback<br>Project discussions among different groups/teams<br>Task assignments across groups/teams |

## Do Nothing Concern

* We have decided to just “Do Nothing” at this point in time

| Cost of the Comfort Zone |                                                                                                                                                                                        |
|:------------------------:|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|         OpEx Cost        | Jenkins Open Source code is free, however, Jenkins implementations are not exactly free….beginning the process today of migrating from Jenkins will reduce your <br>OpEx               |
|       Branding Cost      | Jenkins may open your organization up to security vulnerabilities that could lead to branding vulnerabilities                                                                          |
|     Productivity Cost    | Software delivery with Jenkins CI increased code delivery time through increased build times<br>Maintaining Jenkins is time consuming and manual (requiring manual script development) |

## Migration Time Concern

* Rebuilding my Jenkins CI Pipelines in GitLab will take too much time

|                                                                       Jenkins Wrapper                                                                       |
|:-----------------------------------------------------------------------------------------------------------------------------------------------------------:|
|    Building an automated translator between GitLab and Jenkins would be an ideal solution, which we have extensively evaluated and continue to evaluate.    |
|         At the moment, we’ve determined that the best path forward in moving your CI jobs from Jenkins to GitLab is to implement a Jenkins wrapper.         |
|                              The Wrapper will allow you to put your Jenkins CI jobs within a container and run them in GitLab.                              |
| The Jenkins Wrapper does not allow you to experience the full benefits of GitLab CI, however, it is a positive step forward in your migration plan/strategy |

## Needed Expertise

* I’m not sure my team has the expertise needed to translate our Jenkins CI jobs to GitLab

|                                              Jenkins Migration Services                                              |
|:--------------------------------------------------------------------------------------------------------------------:|
| GitLab has a professional service offering to assist with migrating from legacy CI systems such as Jenkins to GitLab |
|             The Jenkins Migration Services offers three phases:<br>Evaluation<br>Education<br>Empowerment            |

## Functional Parity Concern

* We have extended the functionality of our Jenkins CI tool with Plugins.  I’m concerned that GitLab my not be able to support these added features

|                                                                                             Native Codebase and Open Source                                                                                            |
|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|                                                                      To extend the native functionality of Jenkins you are required to use Plugins                                                                     |
|                                             With GitLab, we do support Plugins but you will find that our core codebase supports many features offered by 3rd party Plugins                                            |
| Furthermore, GitLab Core is Open Source which means that anyone can contribute and add new features/functionality to the codebase which will be merged with the mainline code and automatically tested and maintained. |


