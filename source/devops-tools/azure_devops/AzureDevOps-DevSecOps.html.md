---
layout: markdown_page
title: "Azure DevOps DevSecOps Overview"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

**Azure DevOps** has Application security available through integration with partner products.  They have **no** built-in tools to do SAST, DAST, Container scanning, dependency scanning, or Open Source license compliance scanning.  

In Azure DevOps, the results from non-built-in security scanning tools are not all available from the Merge/Pull Request or pipeline run, and the results are not formatted and presented consistently across the tools.  

You can integrate other tools to Azure Pipelines, but you have to install and maintain each one separately. And every integrated tool (plugin) has a different required configuration to be learned and done.  If not using OSS tools, then there is an additional licensing cost for each new tool.

**GitLab** offers extensive built-in application security scanning.
