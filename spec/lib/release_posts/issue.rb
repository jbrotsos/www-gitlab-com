require 'spec_helper'
require_relative '../../lib/release_posts/issue'

describe ReleasePosts::Issue do
  let(:complete) do
    {
      'idd' => 1,
      'title' => 'Gitaly Cluster',
      'description' => "## Release notes\nBig problem, simple solution!\n\n![](uploads/image.png)\n\nhttps://docs.gitlab.com/ee/administration/gitaly/praefect.html\n\n## Proposal\nwhat we're building\n",
      'labels' => ['feature', 'group::gitaly', 'devops::create', 'category:Gitaly', 'GitLab Premium'],
      'milestone' => {
        'title' => '13.2'
      },
      'assignees' => [
        { 'username' => 'jramsay' },
        { 'username' => 'zj-gitlab' }
      ],
      'web_url' => 'https://gitlab.com/gitlab-org/gitaly/issues/1'
    }
  end
  let(:empty) do
    {
      'idd' => 1,
      'title' => 'Gitaly Cluster',
      'description' => "empty",
      'labels' => [],
      'milestone' => {},
      'assignees' => [],
      'web_url' => 'https://gitlab.com/gitlab-org/gitaly/issues/1'
    }
  end
  let(:issue) { ReleasePosts::Issue.new(complete, 'primary') }
  let(:empty_issue) { ReleasePosts::Issue.new(empty, 'secondary') }

  describe '#group' do
    it 'returns the group that matches the group label' do
      expect(issue.group.key).to eq('gitaly')
    end

    it 'returns nil when no group label is set' do
      expect(empty_issue.group).to be_nil
    end
  end

  describe '#content' do
    it 'returns the extracted content block from the description' do
      expect(issue.content).to eq("Big problem, simple solution!\n")
    end

    it 'returns placeholder content when no content block found' do
      expect(empty_issue.content).to eq("Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit. Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n")
    end
  end

  describe '#documentation_url' do
    it 'returns the first documentation URL from the description' do
      expect(issue.documentation_url).to eq('https://docs.gitlab.com/ee/administration/gitaly/praefect.html')
    end

    it 'returns placeholder ulr when no content block found' do
      expect(empty_issue.documentation_url).to eq('https://docs.gitlab.com/ee/#amazing')
    end
  end

  describe '#image_url' do
    it 'returns the first documentation URL from the description' do
      expect(issue.image_url).to eq('uploads/image.png')
    end

    it 'returns nil when no content block found' do
      expect(empty_issue.image_url).to be_nil
    end
  end
end
